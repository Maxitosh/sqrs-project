from http import HTTPStatus

from django.test import TestCase
from django.urls import reverse


class TestSignUpTests(TestCase):
    def test_sign_up_success(self):
        response = self.client.post(
            reverse("accounts:sign_up"),
            data={"username": "username", "password": "secret_pass"},
        )

        self.assertEqual(response.status_code, HTTPStatus.OK)
