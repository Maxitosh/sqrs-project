from django.contrib.auth.models import User
from django.test import TestCase

from src.accounts.forms import SignIn, SignInViaUsernameForm


class SignInTests(TestCase):
    def test_form_sign_in(self):
        form = SignIn(data={"password": "secret_pass"})

        self.assertEqual(form.data["password"], "secret_pass")

    def test_form_sign_in_remember_me_option(self):
        data = {"password": "secret_pass", "remember_me": False}
        form = SignIn(data=data)

        self.assertEqual(form.data, data)

        data["remember_me"] = True
        form = SignIn(data=data)

        self.assertEqual(form.data, data)

    def test_form_sign_in_empty(self):
        form = SignIn(data={"password": None})

        self.assertEqual(form.errors["password"], ["This field is required."])


class SignInViaUsernameTests(TestCase):
    def test_form_sign_in(self):
        data = {"username": "username", "password": "secret_pass"}
        form = SignInViaUsernameForm(data=data)

        self.assertEqual(form.data, data)

    def test_form_sign_in_remember_me_option(self):
        data = {"username": "username", "password": "secret_pass", "remember_me": True}
        form = SignInViaUsernameForm(data=data)

        self.assertEqual(form.data, data)

        data["remember_me"] = False
        form = SignInViaUsernameForm(data=data)

        self.assertEqual(form.data, data)

    def test_form_sign_in_invalid_username(self):
        User.objects.create(username="username", password="password")
        data = {"username": "username", "password": "secret_pass"}
        form = SignInViaUsernameForm(data=data)

        self.assertEqual(form.errors["password"], ["You entered an invalid password."])
