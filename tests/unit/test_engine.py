from django.contrib.auth.models import User
from django.test import RequestFactory, TestCase
from django.urls import reverse

from src.engine.models import Application
from src.engine.views import (
    ApplicationCreateView,
    ApplicationDeleteView,
    ApplicationListView,
    ApplicationUpdateView,
)


class ResponseTests(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.app = Application()
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username="jacob", email="jacob@…", password="top_secret"
        )

    def test_ApplicationCreateView(self):
        data = {
            "title": "SomeTitle",
            "link": "https://some.link",
            "quarter": 1,
            "justification": "justification",
        }
        request = self.factory.post("create/", data=data)
        request.user = self.user
        response = ApplicationCreateView.as_view()(request)

        self.assertEqual(response.status_code, 302)

    def test_ApplicationCreateView1(self):
        data = {
            "title": "SomeTitle",
            "link": "https://some.link",
            "justification": "justification",
        }
        request = self.factory.post("create/", data=data)
        request.user = self.user
        response = ApplicationCreateView.as_view()(request)

        self.assertEqual(response.status_code, 200)

        # response = ApplicationCreateView().setup(request)

    def test_ApplicationDeleteView(self):
        response = self.client.get(reverse("engine:delete", kwargs={"pk": 1}))

        self.assertEqual(response.status_code, 302)

    def test_ApplicationUpdateView(self):
        response = self.client.get(reverse("engine:update", kwargs={"pk": 1}))

        self.assertEqual(response.status_code, 302)

    def test_ApplicationListView(self):
        request = self.factory.get("list/")
        request.user = self.user

        response = ApplicationListView.as_view()(request)
        self.assertEqual(response.status_code, 200)
