if [ "$VERSION" != "$(cat /root/prod/version)" ]
then
    echo $VERSION > /root/prod/version
    echo new version
    docker-compose -f /root/prod/docker-compose.yaml pull web
    docker-compose -f /root/prod/docker-compose.yaml restart web
fi
